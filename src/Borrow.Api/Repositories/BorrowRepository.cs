﻿using Borrow.Api.Infrastructure;
using Borrow.Api.ViewModels;
using Shared.Models;
using Microsoft.EntityFrameworkCore;

namespace Borrow.Api.Repositories
{
    public class BorrowRepository : IBorrowRepository
    {
        private readonly IBorrowDbContext _dbContext;

        public BorrowRepository(IBorrowDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<Journal>> GetBorrowedBooksAsync()
        {
            var result = await _dbContext.Journals
                .Where(bj => bj.IsReturned == false)
                .ToListAsync();

            return result;
        }

        public async Task<List<BooksBorrowViewModel>> GetMostBorrowedBooksAsync()
        {
            var records = await _dbContext.Journals.ToListAsync();

            return records
            .GroupBy(bj => bj.Book)
            .Select(borrowGroup => new BooksBorrowViewModel(borrowGroup.Key.Id, borrowGroup.Key.Name, borrowGroup.Count()))
            .OrderByDescending(mb => mb.BorrowCount)
            .ToList();

        }

        public async Task<int> GetBorrowedCopiesCountAsync(Guid bookId)
        {
            var result = await _dbContext.Journals
                .Where(bj => bj.IsReturned == false && bj.BookId == bookId)
                .CountAsync();

            return result;
        }

        public async Task<int> GetAvailableCopiesCountAsync(Guid bookId)
        {
            var result = await _dbContext.Catalog
                .Where(s => s.BookId == bookId)
                .Select(s => s.CopyCount)
                .SingleOrDefaultAsync();

            return result;
        }

        public async Task<List<UserRateViewModel>> GetTopReadersAsync(DateTime startDate, DateTime endDate)
        {
            var records = await _dbContext.Journals
                .Where(bj => bj.BorrowDate >= startDate && bj.BorrowDate <= endDate)
                .ToListAsync();

            return records
                .GroupBy(bj => bj.User)
                .Select(borrowGroup => new UserRateViewModel(borrowGroup.Key.Id, borrowGroup.Key.Name, borrowGroup.Count()))
                .ToList();
        }

        public async Task<List<Book>> GetReaderBorrowedBooksAsync(Guid readerId, DateTime startDate, DateTime endDate)
        {
            var result = await _dbContext.Journals
                .Where(bj => bj.UserId == readerId && bj.BorrowDate >= startDate && bj.BorrowDate <= endDate)
                .Select(bj => bj.Book)
                .ToListAsync();

            return result;
        }

        public async Task<List<Book>> GetRecomendedBooksAsync(Guid bookId)
        {
            var readerIds = await _dbContext.Journals
                .Where(bj => bj.BookId == bookId)
                .Select(bj => bj.UserId)
                .ToListAsync();

            var books = await _dbContext.Journals
                .Where(bj => readerIds.Contains(bj.UserId) && bj.BookId != bookId)
                .Select(bj => bj.Book)
                .Distinct()
                .ToListAsync();

            return books;
        }

        public async Task<double> GetBookReadRateAsync(Guid bookId)
        {
            var borrows = await _dbContext.Journals
                .Where(bj => bj.BookId == bookId && bj.IsReturned)
                .ToListAsync();

            var result = await _dbContext.Journals
                .Where(bj => bj.BookId == bookId && bj.IsReturned)
                .AverageAsync(bj => bj.Book.PageCount / (bj.ReturnDate - bj.BorrowDate).Value.Days);

            return result;
        }
    }
}
