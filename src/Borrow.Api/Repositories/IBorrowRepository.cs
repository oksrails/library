﻿using Borrow.Api.ViewModels;
using Shared.Models;

namespace Borrow.Api.Repositories
{
    public interface IBorrowRepository
    {
        Task<List<Journal>> GetBorrowedBooksAsync();
        Task<List<BooksBorrowViewModel>> GetMostBorrowedBooksAsync();
        Task<int> GetBorrowedCopiesCountAsync(Guid bookId);
        Task<int> GetAvailableCopiesCountAsync(Guid bookId);
        Task<List<UserRateViewModel>> GetTopReadersAsync(DateTime startDate, DateTime endDate);
        Task<List<Book>> GetReaderBorrowedBooksAsync(Guid readerId, DateTime startDate, DateTime endDate);
        Task<List<Book>> GetRecomendedBooksAsync(Guid bookId);
        Task<double> GetBookReadRateAsync(Guid bookId);
    }
}
