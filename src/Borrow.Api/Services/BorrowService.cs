﻿using Borrow.Api.Repositories;
using BorrowGrpcService;
using Grpc.Core;

namespace Borrow.Api.Services
{
    public class BorrowService : BorrowGrpcService.Borrow.BorrowBase
    {
        private readonly ILogger<BorrowService> _logger;
        private readonly IBorrowRepository _borrowRepository;

        public BorrowService(IBorrowRepository borrowRepository, ILogger<BorrowService> logger)
        {
            _borrowRepository = borrowRepository;
            _logger = logger;
        }

        public override async Task<GetBorrowedBooksResponse> GetBorrowedBooks(GetBorrowedBooksRequest request, ServerCallContext context)
        {
            var books = await _borrowRepository.GetBorrowedBooksAsync();

            var response = new GetBorrowedBooksResponse();
            books.ForEach(book =>
            {
                response.Books.Add(new Book()
                {
                    Id = book.Id.ToString(),
                    Name = book.Book.Name,
                    PageCount = book.Book.PageCount
                });
            });

            return response;
        }

        public override async Task<GetMostBorrowedBooksResponse> GetMostBorrowedBooks(GetMostBorrowedBooksRequest request, ServerCallContext context)
        {
            var books = await _borrowRepository.GetMostBorrowedBooksAsync();

            if (books != null && books.Any())
            {
                var response = new GetMostBorrowedBooksResponse();
                books.ForEach(book =>
                {
                    response.Books.Add(new Book()
                    {
                        Id = book.BookId.ToString(),
                        Name = book.BookName,
                        PageCount = book.BorrowCount
                    });
                });

                return response;
            }

            context.Status = new Status(StatusCode.NotFound, "Nothing was found.");
            return null;
        }

        public override async Task<GetBorrowedCopiesCountResponse> GetBorrowedCopiesCount(GetBorrowedCopiesCountRequest request, ServerCallContext context)
        {
            if (!Guid.TryParse(request.BookId, out Guid bookId))
            {
                context.Status = new Status(StatusCode.InvalidArgument, "Invalid book id.");
                return null;
            }

            var copyCount = await _borrowRepository.GetBorrowedCopiesCountAsync(bookId);
            return new GetBorrowedCopiesCountResponse() { BorrowCount = copyCount };
        }

        public override async Task<GetAvailebleCopiesCountRespnse> GetAvailebleCopiesCount(GetAvailebleCopiesCountRequest request, ServerCallContext context)
        {
            if (!Guid.TryParse(request.BookId, out Guid bookId))
            {
                context.Status = new Status(StatusCode.InvalidArgument, "Invalid book id.");
                return null;
            }

            var copyCount = await _borrowRepository.GetAvailableCopiesCountAsync(bookId);
            return new GetAvailebleCopiesCountRespnse() { AvailebleCount = copyCount };
        }

        public override async Task<GetTopReadersResponse> GetTopReaders(GetTopReadersRequest request, ServerCallContext context)
        {
            DateTime startDate = request.StartDate.ToDateTime();
            DateTime endDate = request.EndDate.ToDateTime();

            var readers = await _borrowRepository.GetTopReadersAsync(startDate, endDate);

            if (readers != null && readers.Any())
            {
                var response = new GetTopReadersResponse();
                readers.ForEach(user =>
                {
                    response.Users.Add(new User()
                    {
                        Id = user.UserId.ToString(),
                        Name = user.UserName,
                        BookCount = user.BookCount
                    });
                });

                return response;
            }

            context.Status = new Status(StatusCode.NotFound, "Nothing was found.");
            return null;
        }

        public override async Task<GetUserBorrowedBooksResponse> GetUserBorrowedBooks(GetUserBorrowedBooksRequest request, ServerCallContext context)
        {
            if (!Guid.TryParse(request.ReaderId, out Guid readerId))
            {
                context.Status = new Status(StatusCode.InvalidArgument, "Invalid reader id.");
                return null;
            }

            DateTime startDate = request.StartDate.ToDateTime();
            DateTime endDate = request.EndDate.ToDateTime();

            var books = await _borrowRepository.GetReaderBorrowedBooksAsync(readerId, startDate, endDate);

            if (books != null && books.Any())
            {
                var response = new GetUserBorrowedBooksResponse();
                books.ForEach(book =>
                {
                    response.Books.Add(new Book()
                    {
                        Id = book.Id.ToString(),
                        Name = book.Name,
                        PageCount = book.PageCount
                    });
                });

                return response;
            }

            context.Status = new Status(StatusCode.NotFound, "Nothing was found.");
            return null;
        }

        public override async Task<GetRecomendedBooksResponse> GetRecomendedBooks(GetRecomendedBooksRequest request, ServerCallContext context)
        {
            if (!Guid.TryParse(request.BookId, out Guid bookId))
            {
                context.Status = new Status(StatusCode.InvalidArgument, "Invalid book id.");
                return null;
            }

            var books = await _borrowRepository.GetRecomendedBooksAsync(bookId);

            if (books != null && books.Any())
            {
                var response = new GetRecomendedBooksResponse();
                books.ForEach(book =>
                {
                    response.Books.Add(new Book()
                    {
                        Id = book.Id.ToString(),
                        Name = book.Name,
                        PageCount = book.PageCount
                    });
                });

                return response;
            }

            context.Status = new Status(StatusCode.NotFound, "Nothing was found.");
            return null;
        }

        public override async Task<GetBookReadRateResponse> GetBookReadRate(GetBookReadRateRequest request, ServerCallContext context)
        {
            if (!Guid.TryParse(request.BookId, out Guid bookId))
            {
                context.Status = new Status(StatusCode.InvalidArgument, "Invalid book id.");
                return null;
            }

            var readRate = await _borrowRepository.GetBookReadRateAsync(bookId);
            return new GetBookReadRateResponse() { ReadRate = readRate };
        }
    }
}
