﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Shared.Models;

namespace Borrow.Api.Infrastructure
{
    public class CatalogEntityTypeConfiguration
        : IEntityTypeConfiguration<Catalog>
    {
        public void Configure(EntityTypeBuilder<Catalog> builder)
        {
            builder.ToTable("Catalog");

            builder.HasKey(s => s.Id);

            builder.Property(s => s.CopyCount)
                .IsRequired();

            builder.HasOne(s => s.Book)
                .WithMany()
                .HasForeignKey(s => s.BookId);
        }
    }
}
