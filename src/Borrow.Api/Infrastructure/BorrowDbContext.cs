﻿using Microsoft.EntityFrameworkCore;
using Shared.Models;

namespace Borrow.Api.Infrastructure
{
    public class BorrowDbContext : DbContext, IBorrowDbContext
    {
        public BorrowDbContext(DbContextOptions<BorrowDbContext> options) : base(options) { }

        public DbSet<Book> Books { get; set; }
        public DbSet<Catalog> Catalog { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Journal> Journals { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new BookEntityTypeConfiguration());
            builder.ApplyConfiguration(new UserEntityTypeConfiguration());
            builder.ApplyConfiguration(new CatalogEntityTypeConfiguration());
            builder.ApplyConfiguration(new JournalEntityTypeConfiguration());

            builder.Entity<Journal>().Navigation(j => j.Book).AutoInclude();
            builder.Entity<Journal>().Navigation(j => j.User).AutoInclude();
            builder.Entity<Catalog>().Navigation(c => c.Book).AutoInclude();
            base.OnModelCreating(builder);
        }
    }
}
