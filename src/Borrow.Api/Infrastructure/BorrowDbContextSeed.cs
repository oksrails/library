﻿using Shared.Models;

namespace Borrow.Api.Infrastructure
{
    public sealed class BorrowDbContextSeed
    {
        public async Task SeedAsync(IBorrowDbContext dbContext)
        {
            if (dbContext.Books.Any())
                return;

            var books = new List<Book>
            {
                new Book { Id = Guid.NewGuid(), Name = "Book1", PageCount = 300 },
                new Book { Id = Guid.NewGuid(), Name = "Book2", PageCount = 250 },
                new Book { Id = Guid.NewGuid(), Name = "Book3", PageCount = 200 },
                new Book { Id = Guid.NewGuid(), Name = "Book4", PageCount = 400 },
            };

            var users = new List<User>
            {
                new User { Id = Guid.NewGuid(), Name = "Reader1" },
                new User { Id = Guid.NewGuid(), Name = "Reader2" },
                new User { Id = Guid.NewGuid(), Name = "Reader3" },
                new User { Id = Guid.NewGuid(), Name = "Reader4" }
            };

            var catalog = new List<Catalog>();
            Random random = new Random(10);
            int storedCount = 5;
            foreach (var book in books)
            {
                catalog.Add(new Catalog { Id = Guid.NewGuid(), BookId = book.Id, Book = book, CopyCount = ++storedCount });
            }

            var journals = new List<Journal>();
            int i = 0;

            foreach (var user in users)
            {
                foreach (var book in books)
                {
                    i++;
                    journals.Add(new Journal
                    {
                        Id = Guid.NewGuid(),
                        BookId = book.Id,
                        Book = book,
                        UserId = user.Id,
                        User = user,
                        IsReturned = (i % 2 == 0),
                        BorrowDate = DateTime.UtcNow.AddDays(i % 2),
                        ReturnDate = (i % 2 == 0) ? DateTime.UtcNow.AddDays(i) : DateTime.MinValue,
                    });
                }
            }

            journals.Add(new Journal
            {
                Id = Guid.NewGuid(),
                BookId = books[0].Id,
                Book = books[0],
                UserId = users[0].Id,
                User = users[0],
                BorrowDate = DateTime.UtcNow,
                IsReturned = true,
                ReturnDate = DateTime.UtcNow.AddDays(14),
            });
            journals.Add(new Journal
            {
                Id = Guid.NewGuid(),
                BookId = books[1].Id,
                Book = books[1],
                UserId = users[1].Id,
                User = users[1],
                BorrowDate = DateTime.UtcNow,
                IsReturned = true,
                ReturnDate = DateTime.UtcNow.AddDays(14),
            });

            await dbContext.Books.AddRangeAsync(books);
            await dbContext.Users.AddRangeAsync(users);
            await dbContext.Catalog.AddRangeAsync(catalog);
            await dbContext.Journals.AddRangeAsync(journals);

            await dbContext.SaveChangesAsync();
        }
    }
}
