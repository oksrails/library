﻿using Microsoft.EntityFrameworkCore;
using Shared.Models;

namespace Borrow.Api.Infrastructure
{
    public interface IBorrowDbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Catalog> Catalog { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Journal> Journals { get; set; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}
