﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Shared.Models;

namespace Borrow.Api.Infrastructure
{
    public class JournalEntityTypeConfiguration
        : IEntityTypeConfiguration<Journal>
    {
        public void Configure(EntityTypeBuilder<Journal> builder)
        {
            builder.ToTable("Journal");

            builder.HasKey(bj => bj.Id);

            builder.Property(bj => bj.BorrowDate)
                .IsRequired();

            builder.Property(bj => bj.IsReturned)
                .HasDefaultValue(false)
                .IsRequired();

            builder.HasOne(bj => bj.Book)
                .WithMany()
                .HasForeignKey(bj => bj.BookId);

            builder.HasOne(bj => bj.User)
                .WithMany()
                .HasForeignKey(bj => bj.UserId);
        }
    }
}
