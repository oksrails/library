﻿namespace Borrow.Api.ViewModels
{
    public class UserRateViewModel
    {
        public Guid UserId { get; private set; }
        public string UserName { get; private set; }
        public int BookCount { get; private set; }

        public UserRateViewModel(Guid userId, string userName, int bookCount)
        {
            UserId = userId;
            UserName = userName;
            BookCount = bookCount;
        }
    }
}
