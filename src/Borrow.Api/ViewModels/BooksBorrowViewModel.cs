﻿namespace Borrow.Api.ViewModels
{
    public class BooksBorrowViewModel
    {
        public Guid BookId { get; private set; }
        public string BookName { get; private set; }
        public int BorrowCount { get; private set; }

        public BooksBorrowViewModel(Guid bookId, string bookName, int borrowCount)
        {
            BookId = bookId;
            BookName = bookName;
            BorrowCount = borrowCount;
        }
    }
}
