﻿using Google.Protobuf.Collections;
using Shared.Models;

namespace Library.Bff.Mappers
{
    public static class BookMapper
    {
        public static Book MapToModel(BorrowGrpcService.Book book)
        {
            return new Book()
            {
                Id = Guid.Parse(book.Id),
                Name = book.Name,
                PageCount = book.PageCount
            };
        }

        public static List<Book> MapToList(RepeatedField<BorrowGrpcService.Book> books)
        {
            return books.Select(b => MapToModel(b)).ToList();
        }
    }
}
