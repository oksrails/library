﻿using Google.Protobuf.Collections;
using Shared.Models;

namespace Library.Bff.Mappers
{
    public static class UserMapper
    {
        public static User MapToModel(BorrowGrpcService.User user)
        {
            return new User()
            {
                Id = Guid.Parse(user.Id),
                Name = user.Name
            };
        }

        public static List<User> MapToList(RepeatedField<BorrowGrpcService.User> users)
        {
            return users.Select(u => MapToModel(u)).ToList();
        }
    }
}
