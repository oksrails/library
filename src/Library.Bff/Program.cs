using Library.Bff.Extensions;
using Library.Bff.Infrastructure;
using Library.Bff.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
try
{
    builder.Services.AddTransient<GrpcExceptionInterceptor>();
    builder.Services.AddGrpcClient<BorrowGrpcService.Borrow.BorrowClient>(options =>
    {
        options.Address = new Uri(Environment.GetEnvironmentVariable("BORROW_SERVICE_URL"));
    }).AddInterceptor<GrpcExceptionInterceptor>();
    builder.Services.AddTransient<IBorrowService, BorrowService>();
}
catch (Exception ex)
{
    Console.WriteLine(ex);
}

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI();

app.MapEndpoints();

app.Run();