﻿using Library.Bff.Services;

namespace Library.Bff.Extensions
{
    public static class ApiExtensions
    {
        public static WebApplication MapEndpoints(this WebApplication app)
        {
            app.MapGet("/borrowedbooks", async (IBorrowService service) =>
            {
                return await service.GetBorrowedBooksAsync();
            }).WithDisplayName("GetBorrowedBooks");

            app.MapGet("/mostborrowedbooks", async (IBorrowService service) =>
            {
                return await service.GetMostBorrowedBooksAsync();
            }).WithDisplayName("GetMostBorrowedBooks");

            app.MapGet("/borrowedcopies/{bookId}", async (IBorrowService service, Guid bookId) =>
            {
                return await service.GetBorrowedCopiesCountAsync(bookId);
            }).WithDisplayName("GetBorrowedCopyCount");

            app.MapGet("/availeblecopies/{bookId}", async (IBorrowService service, Guid bookId) =>
            {
                return await service.GetAvailebleCopiesCountAsync(bookId);
            }).WithDisplayName("GetAvailebleCopyCount");

            app.MapGet("/topreaders", async (IBorrowService service, DateTime startDate, DateTime endDate) =>
            {
                return await service.GetTopReadersAsync(startDate, endDate);
            }).WithDisplayName("GetTopReaders");

            app.MapGet("/userborrowedbooks/userId", async (IBorrowService service, Guid userId, DateTime startDate, DateTime endDate) =>
            {
                return await service.GetUserBorrowebBooksAsync(userId, startDate, endDate);
            }).WithDisplayName("GetUserBorrowedBooks");

            app.MapGet("/recomendedbooks/{bookId}", async (IBorrowService service, Guid bookId) =>
            {
                return await service.GetRecomendedBooksAsync(bookId);
            }).WithDisplayName("GetRecomendedBoks");

            app.MapGet("/bookreadrate/{bookId}", async (IBorrowService service, Guid bookId) =>
            {
                return await service.GetBookReadRateAsync(bookId);
            }).WithDisplayName("GetBookReadRate");

            return app;
        }
    }
}
