﻿using BorrowGrpcService;
using Google.Protobuf.WellKnownTypes;
using static Library.Bff.Mappers.BookMapper;
using static Library.Bff.Mappers.UserMapper;

namespace Library.Bff.Services
{
    public class BorrowService : IBorrowService
    {
        private readonly BorrowGrpcService.Borrow.BorrowClient _client;
        private readonly ILogger<BorrowService> _logger;

        public BorrowService(BorrowGrpcService.Borrow.BorrowClient client, ILogger<BorrowService> logger)
        {
            _client = client;
            _logger = logger;
        }

        public async Task<List<Shared.Models.Book>> GetBorrowedBooksAsync()
        {
            var request = new GetBorrowedBooksRequest();
            var response = await _client.GetBorrowedBooksAsync(request);
            if (response == null)
                return null;

            return MapToList(response.Books);
        }

        public async Task<List<Shared.Models.Book>> GetMostBorrowedBooksAsync()
        {
            var request = new GetMostBorrowedBooksRequest();
            var response = await _client.GetMostBorrowedBooksAsync(request);
            if (response == null)
                return null;

            return MapToList(response.Books);
        }

        public async Task<int> GetBorrowedCopiesCountAsync(Guid bookId)
        {
            var request = new GetBorrowedCopiesCountRequest() { BookId = bookId.ToString() };
            var response = await _client.GetBorrowedCopiesCountAsync(request);

            return response.BorrowCount;
        }

        public async Task<int> GetAvailebleCopiesCountAsync(Guid bookId)
        {
            var request = new GetAvailebleCopiesCountRequest() { BookId = bookId.ToString() };
            var response = await _client.GetAvailebleCopiesCountAsync(request);

            return response.AvailebleCount;
        }

        public async Task<List<Shared.Models.User>> GetTopReadersAsync(DateTime startDate, DateTime endDate)
        {
            var request = new GetTopReadersRequest() { StartDate = Timestamp.FromDateTime(startDate.ToUniversalTime()), EndDate = Timestamp.FromDateTime(endDate.ToUniversalTime()) };
            var response = await _client.GetTopReadersAsync(request);
            if (response == null)
                return null;

            return MapToList(response.Users);
        }

        public async Task<List<Shared.Models.Book>> GetUserBorrowebBooksAsync(Guid userId, DateTime startDate, DateTime endDate)
        {
            var requset = new GetUserBorrowedBooksRequest() 
            { 
                ReaderId = userId.ToString(), 
                StartDate = Timestamp.FromDateTime(startDate.ToUniversalTime()), 
                EndDate = Timestamp.FromDateTime(endDate.ToUniversalTime()) 
            };
            var response = await _client.GetUserBorrowedBooksAsync(requset);
            if (response == null)
                return null;

            return MapToList(response.Books);
        }

        public async Task<List<Shared.Models.Book>> GetRecomendedBooksAsync(Guid bookId)
        {
            var request = new GetRecomendedBooksRequest() { BookId = bookId.ToString() };
            var response = await _client.GetRecomendedBooksAsync(request);
            if (response == null)
                return null;

            return MapToList(response.Books);
        }

        public async Task<double> GetBookReadRateAsync(Guid bookId)
        {
            var request = new GetBookReadRateRequest() { BookId = bookId.ToString() };
            var response = await _client.GetBookReadRateAsync(request);

            return Convert.ToDouble(response.ReadRate);
        }
    }
}
