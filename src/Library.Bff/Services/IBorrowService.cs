﻿using Shared.Models;

namespace Library.Bff.Services
{
    public interface IBorrowService
    {
        Task<List<Book>> GetBorrowedBooksAsync();
        Task<List<Book>> GetMostBorrowedBooksAsync();
        Task<int> GetBorrowedCopiesCountAsync(Guid bookId);
        Task<int> GetAvailebleCopiesCountAsync(Guid bookId);
        Task<List<User>> GetTopReadersAsync(DateTime startDate, DateTime endDate);
        Task<List<Book>> GetUserBorrowebBooksAsync(Guid userId, DateTime startDate, DateTime endDate);
        Task<List<Book>> GetRecomendedBooksAsync(Guid bookId);
        Task<double> GetBookReadRateAsync(Guid bookId);
    }
}
