﻿namespace Shared.Models
{
    public class Catalog
    {
        public Guid Id { get; set; }
        public Guid BookId { get; set; }
        public virtual Book Book { get; set; }
        public int CopyCount { get; set; }
    }
}
