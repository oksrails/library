﻿namespace Shared.Models
{
    public class Journal
    {
        public Guid Id { get; set; }
        public Guid BookId { get; set; }
        public virtual Book Book { get; set; }
        public Guid UserId { get; set; }
        public virtual User User { get; set; }
        public bool IsReturned { get; set; }
        public DateTime BorrowDate { get; set; }
        public DateTime? ReturnDate { get; set; }
    }
}
