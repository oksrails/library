﻿using Borrow.Api.Infrastructure;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Threading.Tasks;
using Shared.Models;
using Moq.EntityFrameworkCore;

namespace Borrow.Api.Tests
{
    public static class MockData
    {
        public static IBorrowDbContext GetMockDbContext()
        {
            var books = new List<Book>
            {
                new Book { Id = Guid.NewGuid(), Name = "Book1", PageCount = 300 },
                new Book { Id = Guid.NewGuid(), Name = "Book2", PageCount = 250 },
                new Book { Id = Guid.NewGuid(), Name = "Book3", PageCount = 200 },
                new Book { Id = Guid.NewGuid(), Name = "Book4", PageCount = 400 },
            };

            var readers = new List<User>
            {
                new User { Id = Guid.NewGuid(), Name = "Reader1" },
                new User { Id = Guid.NewGuid(), Name = "Reader2" },
                new User { Id = Guid.NewGuid(), Name = "Reader3" },
                new User { Id = Guid.NewGuid(), Name = "Reader4" }
            };

            var storages = new List<Catalog>();
            Random random = new Random(10);
            int storedCount = 5;
            foreach (var book in books)
            {
                storages.Add(new Catalog { Id = Guid.NewGuid(), BookId = book.Id, Book = book, CopyCount = ++storedCount });
            }

            var borrowJournals = new List<Journal>();
            int i = 0;

            foreach (var User in readers)
            {
                foreach (var book in books)
                {
                    i++;
                    borrowJournals.Add(new Journal
                    {
                        Id = Guid.NewGuid(),
                        BookId = book.Id,
                        Book = book,
                        UserId = User.Id,
                        User = User,
                        IsReturned = (i % 2 == 0),
                        BorrowDate = DateTime.UtcNow.AddDays(i % 2),
                        ReturnDate = (i % 2 == 0) ? DateTime.UtcNow.AddDays(i) : DateTime.MinValue,
                    });
                }
            }

            borrowJournals.Add(new Journal
            {
                Id = Guid.NewGuid(),
                BookId = books[0].Id,
                Book = books[0],
                UserId = readers[0].Id,
                User = readers[0],
                BorrowDate = DateTime.UtcNow,
                IsReturned = true,
                ReturnDate = DateTime.UtcNow.AddDays(14),
            });
            borrowJournals.Add(new Journal
            {
                Id = Guid.NewGuid(),
                BookId = books[1].Id,
                Book = books[1],
                UserId = readers[1].Id,
                User = readers[1],
                BorrowDate = DateTime.UtcNow,
                IsReturned = true,
                ReturnDate = DateTime.UtcNow.AddDays(14),
            });

            var mock = new Mock<IBorrowDbContext>();
            mock.Setup(m => m.Books).ReturnsDbSet(books);
            mock.Setup(m => m.Users).ReturnsDbSet(readers);
            mock.Setup(m => m.Catalog).ReturnsDbSet(storages);
            mock.Setup(m => m.Journals).ReturnsDbSet(borrowJournals);
            return mock.Object;
        }

    }
}
