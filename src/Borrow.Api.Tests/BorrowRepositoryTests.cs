using Borrow.Api.Infrastructure;
using Borrow.Api.Repositories;

namespace Borrow.Api.Tests
{
    public class BorrowRepositoryTests
    {
        private IBorrowDbContext _dbContext;
        private BorrowRepository _repository;
        public BorrowRepositoryTests()
        {
            _dbContext = MockData.GetMockDbContext();
            _repository = new BorrowRepository(_dbContext);
        }

        [Fact]
        public async void Get_borrowed_books_test()
        {
            var result = await _repository.GetBorrowedBooksAsync();

            Assert.NotNull(result);
            Assert.Equal(8, result.Count);
        }

        [Fact]
        public async void Get_most_borrowed_books_test()
        {
            var result = await _repository.GetMostBorrowedBooksAsync();
            result[0].BookName.Should().Be("Book1");
            result[1].BookName.Should().Be("Book2");
        }

        [Fact]
        public async void Get_borrowed_copy_count_test()
        {
            Guid bookId = _dbContext.Books.SingleOrDefault(b => b.Name == "Book1").Id;
            int count = await _repository.GetBorrowedCopiesCountAsync(bookId);
            count.Should().Be(4);
        }

        [Fact]
        public async void Get_availeble_copies_count_test()
        {
            Guid bookId = _dbContext.Books.SingleOrDefault(b => b.Name == "Book1").Id;
            int count = await _repository.GetAvailableCopiesCountAsync(bookId);
            count.Should().Be(6);
        }

        [Fact]
        public async void Get_top_readers_test()
        {
            DateTime startDate = DateTime.UtcNow.AddHours(-4);
            DateTime endDate = DateTime.UtcNow.AddHours(12);
            var result = await _repository.GetTopReadersAsync(startDate, endDate);
            result[0].UserName.Should().Be("Reader1");
        }

        [Fact]
        public async void Get_reader_books_test()
        {

            Guid readerId = _dbContext.Users.SingleOrDefault(r => r.Name == "Reader1").Id;
            DateTime startDate = DateTime.UtcNow.AddHours(-4);
            DateTime endDate = DateTime.UtcNow.AddHours(12);
            var result = await _repository.GetReaderBorrowedBooksAsync(readerId, startDate, endDate);
            result.Count.Should().Be(3);
        }

        [Fact]
        public async void Get_recomended_books_test()
        {
            Guid bookId = _dbContext.Books.SingleOrDefault(b => b.Name == "Book1").Id;
            var result = await _repository.GetRecomendedBooksAsync(bookId);
            result.Count.Should().Be(3);
        }

        [Fact]
        public async void Get_book_read_rate_test()
        {
            Guid bookId = _dbContext.Books.SingleOrDefault(b => b.Name == "Book1").Id;
            var result = await _repository.GetBookReadRateAsync(bookId);
            result.Should().Be(21);
        }
    }
}